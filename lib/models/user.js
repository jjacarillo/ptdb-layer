// user model
var dbHelper = require('../helpers/database');
var crypt_decrypt = require('../helpers/crypt_decrypt');


var UserSchema = {
  email: {
    type: String,
    lowercase: true,
    unique: true
  },
  lastName: {
    type: String
  },
  firstName: {
    type: String
  },
  isPem: {
    type: Boolean
  },
  pem: {
    type: String
  },
  pwd: {
    type: String
  }
};


var User = dbHelper.createSchemaModel('User', UserSchema);

module.exports.create = function(obj, callback) {
  var user = new User();
  // enforce schema
  for(prop in obj) {
    if(UserSchema[prop] != undefined && UserSchema[prop] != null) {
      user[prop] = obj[prop];
    }
  }
  
  user['pwd'] = crypt_decrypt.cryptme( user['pwd'] );
  user.save(function(err, user) {
    if(err){
      callback(err, null);
    }
    delete user['pwd']
    callback(null, user);
  });

}

module.exports.update = function(obj, callback) {
  for(prop in obj) {
    if(UserSchema[prop] === null) {
      delete obj[prop];
    }
  }

  var _id = obj['_id'];
  delete obj['_id'];
  var options = {};

  User.findByIdAndUpdate(_id, obj, options, function(err, count) {
    if(err){
      callback(err, null);
    }

    callback(null, count);
  });

};;

module.exports.find = function(obj, callback) {
  for(prop in obj) {
    if(UserSchema[prop] === undefined) {
      delete obj[prop];
    }
  }
  if ( obj['pwd'] != '' && obj['pwd'] != undefined && obj['pwd'] != null)
    obj['pwd'] = crypt_decrypt.cryptme(obj['pwd']);
  console.log(obj);
  User.find(obj, function(err, users) {
    if(err){
      callback(err, null);
    }

    callback(null, users);
  });

};

module.exports.remove = function(obj, callback) {
  for(prop in obj) {
    if(UserSchema[prop] === null) {
      delete obj[prop];
    }
  }

  User.remove(obj, function(err, count) {
    if(err){
      callback(err, null);
    }

    callback(null, count);
  });

};

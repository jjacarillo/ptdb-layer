// track model
var mongoose = require('mongoose');

var dbHelper = require('../helpers/database');

var TrackSchema = {
  date: {
    type: String
  },
  location: {
    type: mongoose.Schema.ObjectId,
    ref: 'Location'
  },
  user: {
    type: mongoose.Schema.ObjectId,
    ref: 'User'
  }
};


var Track = dbHelper.createSchemaModel('Track', TrackSchema);

module.exports.create = function(obj, callback) {
  var track = new Track();
  // enforce schema
  for(prop in obj) {
    if(TrackSchema[prop] != null) {
      track[prop] = obj[prop];
    }
  }

  track.save(function(err, track) {
    if(err){
      callback(err, null);
    }

    callback(null, track);
  });

}

module.exports.update = function(obj, callback) {
  for(prop in obj) {
    // do not allow date to be updated
    if(TrackSchema[prop] === null || prop == 'date') {
      delete obj[prop];
    }
  }

  var _id = obj['_id'];
  delete obj['_id'];
  var options = {};

  Track.findByIdAndUpdate(_id, obj, options, function(err, count) {
    if(err){
      callback(err, null);
    }

    callback(null, count);
  });

};;

module.exports.find = function(obj, callback) {
  for(prop in obj) {
    if(TrackSchema[prop] === null) {
      delete obj[prop];
    }
  }

  Track.find(obj).populate('user').populate('location').exec(function(err, tracks) {
    if(err){
      callback(err, null);
    }

    callback(null, tracks);
  });

};

module.exports.remove = function(obj, callback) {
  for(prop in obj) {
    if(TrackSchema[prop] === null) {
      delete obj[prop];
    }
  }

  Track.remove(obj, function(err, count) {
    if(err){
      callback(err, null);
    }

    callback(null, count);
  });

};

// location  model
var dbHelper = require('../helpers/database');

var LocationSchema = {
  name: {
    type: String,
    uppercase: true,
    unique: true
  }
};


var Location = dbHelper.createSchemaModel('Location', LocationSchema);

module.exports.create = function(obj, callback) {
  var location = new Location();
  // enforce schema
  for(prop in obj) {
    if(LocationSchema[prop] != null) {
      location[prop] = obj[prop];
    }
  }

  location.save(function(err, location) {
    if(err){
      callback(err, null);
    }

    callback(null, location);
  });

}

module.exports.update = function(obj, callback) {
  for(prop in obj) {
    if(LocationSchema[prop] === null) {
      delete obj[prop];
    }
  }

  var _id = obj['_id'];
  delete obj['_id'];
  var options = {};

  Location.findByIdAndUpdate(_id, obj, options, function(err, count) {
    if(err){
      callback(err, null);
    }

    callback(null, count);
  });

};;

module.exports.find = function(obj, callback) {
  for(prop in obj) {
    if(LocationSchema[prop] === null) {
      delete obj[prop];
    }
  }

  Location.find(obj, function(err, locations) {
    if(err){
      callback(err, null);
    }

    callback(null, locations);
  });

};

module.exports.remove = function(obj, callback) {
  for(prop in obj) {
    if(LocationSchema[prop] === null) {
      delete obj[prop];
    }
  }

  Location.remove(obj, function(err, count) {
    if(err){
      callback(err, null);
    }

    callback(null, count);
  });

};

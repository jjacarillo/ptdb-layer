// Bluemix VCAP Services
var services = JSON.parse(process.env.VCAP_SERVICES || "{}");

// return mongodb URL
module.exports.getDbConnStr = function(dbIndex) {

	// default to first db service found in Bluemix VCAP_SERVICES if index
	// is not specified
	if(dbIndex === undefined) {
			dbIndex = 0;
	}

	try {
		// in production/staging env (Bluemix)
		return Object.keys(services).length > 0 ? services['mongodb-2.2'][dbIndex]['credentials']['url'] : 'mongodb://localhost/practitionertracker';
	} catch(e) {
		console.log(e);
		// fallback to localhost
		return 'mongodb://localhost/practitionertracker';
	}
};

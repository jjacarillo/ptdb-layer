(function() {
  var clean, urlparser;

  urlparser = require('url');

  clean = function(str) {
    return str.replace(/<[A-Za-z0-9_\/]+>/g, ' ');
  };

  module.exports.parse = function(url, bool) {
    var parsed, property, value, cleanValue, _ref;
    parsed = urlparser.parse(url, bool);
    if ((parsed != null ? parsed.query : void 0) != null) {
      _ref = parsed.query;
      for (property in _ref) {
        value = _ref[property];
        delete parsed.query[property];
        property = clean(property);
        cleanValue = clean(value);
        if (cleanValue == "true" || cleanValue == "false"){
          cleanValue =  (cleanValue === "true")
        }
        parsed.query[property] = cleanValue;
      }
    }
    return parsed;
  };

}).call(this);

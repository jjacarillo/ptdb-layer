var mongoose = require('mongoose'),
		Schema = mongoose.Schema,
    moment = require('moment');

var config = require('../config');

// connect to default db
var DB = config.getDbConnStr();
var conn = mongoose.connect(DB);

module.exports.createSchemaModel = function(modelName, schemaJson) {
	var schema = new Schema(schemaJson);
	return conn.model(modelName, schema);
};

module.exports.formatDateString = function(dateStr) {
  var m = moment(dateStr);
  return m.format('YYYYMMDD');
};

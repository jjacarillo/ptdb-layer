(function() {

  var crypto = require('crypto');
  var assert = require('assert');
  
  var algorithm = 'aes256';           // or any other algorithm supported by OpenSSL
  var key = 'itsaboy';
  var text = 'I love kittens';
  
  module.exports.cryptme = function(text) {
    var encrypted = ''
    if (text!='' && text!=null && text!=undefined){
      var cipher = crypto.createCipher(algorithm, key);  
      encrypted = cipher.update(text, 'utf8', 'hex') + cipher.final('hex');
    }
    return encrypted;
  };
  
  module.exports.decryptme = function(encrypted) {
    var decrypted = ''
    if (encrypted!='' && encrypted!=null && encrypted!=undefined){
      var decipher = crypto.createDecipher(algorithm, key);
      decrypted = decipher.update(encrypted, 'hex', 'utf8') + decipher.final('utf8');
    }
    return decrypted;
  };
  
}).call(this);
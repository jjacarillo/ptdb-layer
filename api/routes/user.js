var User      = require('../../lib/models/user');
var urlParser = require('../../lib/helpers/urlparser');


module.exports = function(app) {

  // full restful CRUD endpoints
  app.get('/users', findAll);
  app.del('/users', removeAll);

  app.get('/users/:email', findOne);
  app.put('/users', create);
  app.post('/users/:_id', update);
  app.del('/users/:_id', remove);

  function create(req, res, next) {
    var status, message, error, user;
    console.log('create user');
    User.create(req.params, function(err, user) {
      if(err) {
        status = 'error';
        message = 'An error occured while creating user.';
        error = err;
      } else {
        status = 'ok';
        message = 'User successfully created.';
        error = null;
      }

      response = {
        status: status,
        message: message,
        error: error,
        user: user
      };
      res.send(response);
      next();
    });
  }

  function update(req, res, next) {
    var status, message, error, user;
    User.update(req.params, function(err, user) {
      if(err) {
        status = 'error';
        message = 'An error occured while updating user.';
        error = err;
        user = null;
      } else {
        status = 'ok';
        message = 'User successfully updated.';
        error = null;
      }

      response = {
        status: status,
        message: message,
        error: error,
        user: user 
      };
      res.send(response);
      next();
    });
  }

  function findAll(req, res, next) {
    var status, message, error, users;
    parsed = urlParser.parse(req.url + '?' +req.body, true);
    console.log('findAll invoke');
    console.log(parsed.query);
    User.find(parsed.query, function(err, users) {
      if(err) {
        status = 'error';
        message = 'An error occurred while finding all users.';
        error = err;
        users = null;
      } else {
        status = 'ok';
        message = 'Query for all users successful.';
        error = err;
      }

      response = {
        status: status,
        message: message,
        error: error,
        users: users
      };
      res.send(response);

    });
  }

  function findOne(req, res, next) {
    var status, message, error, users;
    var query = {_id:req.params._id_email, email: req.params._id_email}
    console.log('find one');
    console.log(query);
    User.find(query, function(err, users) {
      if(err) {
        status = 'error';
        message = 'An error occurred while finding user.';
        error = err;
        users = [];
      } else {
        status = 'ok';
        message = 'Query for specified user successful.';
        error = err;
      }

      response = {
        status: status,
        message: message,
        error: error,
        user: users[0]
      };
      res.send(response);

    });
  }
  function removeAll(req, res, next) {
    var status, message, error, count;
    User.remove({}, function(err, count) {
      if(err) {
        status = 'error';
        message = 'An error occurred while removing all users.';
        error = err;
        count = 0;
      } else {
        status = 'ok';
        message = 'All users successfully deleted.';
        error = err;
      }

      response = {
        status: status,
        message: message,
        error: error,
        count: count
      };
      res.send(response);

    });
  }

  function remove(req, res, next) {
    var status, message, error, count;
    User.remove(req.params, function(err, count) {
      if(err) {
        status = 'error';
        message = 'An error occurred while removing user.';
        error = err;
        count = 0;
      } else {
        status = 'ok';
        message = 'User successfully deleted.';
        error = err;
      }

      response = {
        status: status,
        message: message,
        error: error,
        count: count
      };
      res.send(response);

    });
  }

};

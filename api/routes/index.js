// Public API endpoints
module.exports = function(app) {

  app.get('/', index);

  function index(req, res, next) {
    response = {
      status: 'ok',
      message: 'Practitioner Tracker Restful Database Service'
    };
    res.send(response);
    next();
  }

  // require all other routes
  require('./user.js')(app);
  require('./location.js')(app);
  require('./track.js')(app);
};

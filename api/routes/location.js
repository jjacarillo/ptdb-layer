var Location = require('../../lib/models/location');

module.exports = function(app) {

  // full restful CRUD endpoints
  app.get('/locations', findAll);
  app.del('/locations', removeAll);

  app.get('/locations/:_id', findOne);
  app.put('/locations', create);
  app.post('/locations/:_id', update);
  app.del('/locations/:_id', remove);

  function create(req, res, next) {
    var status, message, error, location;
    Location.create(req.params, function(err, location) {
      if(err) {
        status = 'error';
        message = 'An error occured while creating location.';
        error = err;
      } else {
        status = 'ok';
        message = 'Location successfully created.';
        error = null;
      }

      response = {
        status: status,
        message: message,
        error: error,
        location: location
      };
      res.send(response);
      next();
    });
  }

  function update(req, res, next) {
    var status, message, error, location;
    Location.update(req.params, function(err, location) {
      if(err) {
        status = 'error';
        message = 'An error occured while updating location.';
        error = err;
        location = null;
      } else {
        status = 'ok';
        message = 'Location successfully updated.';
        error = null;
      }

      response = {
        status: status,
        message: message,
        error: error,
        location: location 
      };
      res.send(response);
      next();
    });
  }

  function findAll(req, res, next) {
    var status, message, error, locations;
    Location.find({}, function(err, locations) {
      if(err) {
        status = 'error';
        message = 'An error occurred while finding all locations.';
        error = err;
        locations = null;
      } else {
        status = 'ok';
        message = 'Query for all locations successful.';
        error = err;
      }

      response = {
        status: status,
        message: message,
        error: error,
        locations: locations
      };
      res.send(response);

    });
  }

  function findOne(req, res, next) {
    var status, message, error, locations;
    Location.find(req.params, function(err, locations) {
      if(err) {
        status = 'error';
        message = 'An error occurred while finding location.';
        error = err;
        locations = [];
      } else {
        status = 'ok';
        message = 'Query for specified location successful.';
        error = err;
      }

      response = {
        status: status,
        message: message,
        error: error,
        location: locations[0]
      };
      res.send(response);

    });
  }
  function removeAll(req, res, next) {
    var status, message, error, count;
    Location.remove({}, function(err, count) {
      if(err) {
        status = 'error';
        message = 'An error occurred while removing all locations.';
        error = err;
        count = 0;
      } else {
        status = 'ok';
        message = 'All locations successfully deleted.';
        error = err;
      }

      response = {
        status: status,
        message: message,
        error: error,
        count: count
      };
      res.send(response);

    });
  }

  function remove(req, res, next) {
    var status, message, error, count;
    Location.remove(req.params, function(err, count) {
      if(err) {
        status = 'error';
        message = 'An error occurred while removing location.';
        error = err;
        count = 0;
      } else {
        status = 'ok';
        message = 'Location successfully deleted.';
        error = err;
      }

      response = {
        status: status,
        message: message,
        error: error,
        count: count
      };
      res.send(response);

    });
  }

};

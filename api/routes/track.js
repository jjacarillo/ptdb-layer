var Track = require('../../lib/models/track');

module.exports = function(app) {

  // full restful CRUD endpoints
  app.get('/tracks', findAll);
  app.post('/tracks', findAll);
  app.del('/tracks', removeAll);

  app.get('/tracks/:date', findDate);
  app.get('/tracks/:date/:userId', findDate);
  app.put('/tracks', create);
  app.post('/tracks/:_id', update);
  app.del('/tracks/:_id', remove);

  function create(req, res, next) {
    var status, message, error, track;
	console.log(req.params);
    Track.create(req.params, function(err, track) {
      if(err) {
        status = 'error';
        message = 'An error occured while creating track.';
        error = err;
      } else {
        status = 'ok';
        message = 'Track successfully created.';
        error = null;
      }

      response = {
        status: status,
        message: message,
        error: error,
        track: track
      };
      res.send(response);
      next();
    });
  }

  function update(req, res, next) {
    var status, message, error, track;
    Track.update(req.params, function(err, track) {
      if(err) {
        status = 'error';
        message = 'An error occured while updating track.';
        error = err;
        track = null;
      } else {
        status = 'ok';
        message = 'Track successfully updated.';
        error = null;
      }

      response = {
        status: status,
        message: message,
        error: error,
        track: track 
      };
      res.send(response);
      next();
    });
  }

  function findAll(req, res, next) {
    var status, message, error, tracks;
	console.log(req.body);
    Track.find(req.body, function(err, tracks) {
      if(err) {
        status = 'error';
        message = 'An error occurred while finding all tracks.';
        error = err;
        tracks = null;
      } else {
        status = 'ok';
        message = 'Query for all tracks successful.';
        error = err;
      }

      response = {
        status: status,
        message: message,
        error: error,
        tracks: tracks
      };
      res.send(response);

    });
  }

  function findDate(req, res, next) {
    var status, message, error, tracks;
	console.log(req.params);
    Track.find(req.params, function(err, tracks) {
      if(err) {
        status = 'error';
        message = 'An error occurred while finding tracks.';
        error = err;
        tracks = [];
      } else {
        status = 'ok';
        message = 'Query for specified date successful.';
        error = err;
      }

      response = {
        status: status,
        message: message,
        error: error,
        tracks: tracks
      };
      res.send(response);

    });
  }
  function removeAll(req, res, next) {
    var status, message, error, count;
    Track.remove({}, function(err, count) {
      if(err) {
        status = 'error';
        message = 'An error occurred while removing all tracks.';
        error = err;
        count = 0;
      } else {
        status = 'ok';
        message = 'All tracks successfully deleted.';
        error = err;
      }

      response = {
        status: status,
        message: message,
        error: error,
        count: count
      };
      res.send(response);

    });
  }

  function remove(req, res, next) {
    var status, message, error, count;
    Track.remove(req.params, function(err, count) {
      if(err) {
        status = 'error';
        message = 'An error occurred while removing track.';
        error = err;
        count = 0;
      } else {
        status = 'ok';
        message = 'Track successfully deleted.';
        error = err;
      }

      response = {
        status: status,
        message: message,
        error: error,
        count: count
      };
      res.send(response);

    });
  }

};

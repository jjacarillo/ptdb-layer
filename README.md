INSTALLATION
------------

1. Install [node](http://nodejs.org/)

2. Checkout the code: `git clone https://hub.jazz.net/git/jjacarillo/pt-db-layer`

3. Install the node modules/dependencies:  `npm install`

4. Run test scripts: `npm test`

'use strict';

var should  = require('should');
var assert  = require('assert');
var request = require('supertest');

describe('Practitioner Tracker DB API', function() {
	var url = 'localhost:3000';

	it('getting API index', function(done) {
    console.log('*******************************************');
		console.log('Getting response from API base endpoint');
		var query = {};
		request(url).get('/').send(query).end(function(err, res) {
			if(err) {
				throw err;
			}
			console.log(res.body);
			(res.body.status || '').should.equal('ok');
			(res.body.message || false).should.not.equal(false);
			done();
		});
	});

  // BEGIN: Test user resource
	
	it('get list of all users', function(done) {
		console.log('*******************************************');
  		console.log(' *           TEST USER RESOURCE            *');
  		console.log(' *******************************************');
		console.log('Getting all users');
		var query = {};
		request(url).get('/users').send(query).end(function(err, res) {
			if(err) {
				throw err;
			}
			console.log(res.body);
			(res.body.status || '').should.equal('ok');
			(res.body.message || false).should.not.equal(false);
			(res.body.users instanceof Array).should.equal(true);
			done();
		});
	});

	it('delete all users', function(done) {
    console.log('*******************************************');
		console.log('Deleting all users');
		var query = {};
		request(url).delete('/users').send(query).end(function(err, res) {
			if(err) {
				throw err;
			}
			console.log(res.body);
			(res.body.status || '').should.equal('ok');
			(res.body.message || false).should.not.equal(false);
			done();
		});
	});

  var userId;
	it('create user', function(done) {
    console.log('*******************************************');
		console.log('Creating new user');
		var query = {
        lastName: "Carillo",
        firstName: "Jesse",
        email: "carillja@ph.ibm.com",
        isPem: false,
        pem: "ndeguzman@ph.ibm.com"
    };
		request(url).put('/users').send(query).end(function(err, res) {
			if(err) {
				throw err;
			}
			console.log(res.body);
			(res.body.status || '').should.equal('ok');
			(res.body.message || false).should.not.equal(false);
			(res.body.user._id ? res.body.user._id : 0).should.not.equal(0);
      userId = (res.body.user._id ? res.body.user._id : 0);
			done();
		});
	});

	it('get user', function(done) {
    console.log('*******************************************');
		console.log('Getting user: ' + userId);
		var query = {};
		request(url).get('/users/' + userId).send(query).end(function(err, res) {
			if(err) {
				throw err;
			}
			console.log(res.body);
			(res.body.status || '').should.equal('ok');
			(res.body.message || false).should.not.equal(false);
			(res.body.user._id ? res.body.user._id : 0).should.not.equal(0);
			done();
		});
	});

	it('update user', function(done) {
    console.log('*******************************************');
		console.log('Updating user: ' + userId);
		var query = {
      lastName: "Carillo",
      firstName: "Jesse Jasper",
      email: "carillja@ph.ibm.com",
      isPem: false,
      pem: "ndeguzman@ph.ibm.com"
    };
		request(url).post('/users/' + userId).send(query).end(function(err, res) {
			if(err) {
				throw err;
			}
			console.log(res.body);
			(res.body.status || '').should.equal('ok');
			(res.body.message || false).should.not.equal(false);
			(res.body.user._id ? res.body.user._id : 0).should.not.equal(0);
			done();
		});
	});

	it('delete user', function(done) {
    console.log('*******************************************');
		console.log('Deleting user: ' + userId);
		var query = {};
		request(url).delete('/users/' + userId).send(query).end(function(err, res) {
			if(err) {
				throw err;
			}
			console.log(res.body);
			(res.body.status || '').should.equal('ok');
			(res.body.message || false).should.not.equal(false);
			done();
		});
	});

  // BEGIN: Test location resource
	
	it('get list of all locations', function(done) {
		console.log('');
		console.log('');
		console.log('');
		console.log('*******************************************');
	  	console.log('*         TEST LOCATION RESOURCE          *');
	  	console.log('*******************************************');
		console.log('Getting all locations');
		var query = {};
		request(url).get('/locations').send(query).end(function(err, res) {
			if(err) {
				throw err;
			}
			console.log(res.body);
			(res.body.status || '').should.equal('ok');
			(res.body.message || false).should.not.equal(false);
			(res.body.locations instanceof Array).should.equal(true);
			done();
		});
	});

	it('delete all locations', function(done) {
    console.log('*******************************************');
		console.log('Deleting all locations');
		var query = {};
		request(url).delete('/locations').send(query).end(function(err, res) {
			if(err) {
				throw err;
			}
			console.log(res.body);
			(res.body.status || '').should.equal('ok');
			(res.body.message || false).should.not.equal(false);
			done();
		});
	});

  var locId;
	it('create location', function(done) {
    console.log('*******************************************');
		console.log('Creating new location');
		var query = {
        name: "Cebu",
    };
		request(url).put('/locations').send(query).end(function(err, res) {
			if(err) {
				throw err;
			}
			console.log(res.body);
			(res.body.status || '').should.equal('ok');
			(res.body.message || false).should.not.equal(false);
			(res.body.location._id ? res.body.location._id : 0).should.not.equal(0);
      locId = (res.body.location._id ? res.body.location._id : 0);
			done();
		});
	});

	it('get location', function(done) {
    console.log('*******************************************');
		console.log('Getting location: ' + locId);
		var query = {};
		request(url).get('/locations/' + locId).send(query).end(function(err, res) {
			if(err) {
				throw err;
			}
			console.log(res.body);
			(res.body.status || '').should.equal('ok');
			(res.body.message || false).should.not.equal(false);
			(res.body.location._id ? res.body.location._id : 0).should.not.equal(0);
			done();
		});
	});

	it('update location', function(done) {
    console.log('*******************************************');
		console.log('Updating location: ' + locId);
		var query = {
      name: "Cebu (TGU)",
    };
		request(url).post('/locations/' + locId).send(query).end(function(err, res) {
			if(err) {
				throw err;
			}
			console.log(res.body);
			(res.body.status || '').should.equal('ok');
			(res.body.message || false).should.not.equal(false);
			(res.body.location._id ? res.body.location._id : 0).should.not.equal(0);
			done();
		});
	});

	it('delete location', function(done) {
    console.log('*******************************************');
		console.log('Deleting location: ' + locId);
		var query = {};
		request(url).delete('/locations/' + locId).send(query).end(function(err, res) {
			if(err) {
				throw err;
			}
			console.log(res.body);
			(res.body.status || '').should.equal('ok');
			(res.body.message || false).should.not.equal(false);
			done();
		});
	});

		// BEGIN: Test tracks resource
		
    it('get list of all tracks', function(done) {
        console.log('');
        console.log('');
        console.log('');
        console.log('*******************************************');
        console.log('*         TEST TRACKER RESOURCE           *');
        console.log('*******************************************');
        console.log('Getting all tracks');
        var query = {};
        request(url).get('/tracks').send(query).end(function(err, res) {
            if(err) {
                throw err;
            }
            console.log(res.body);
            (res.body.status || '').should.equal('ok');
            (res.body.message || false).should.not.equal(false);
            (res.body.tracks instanceof Array).should.equal(true);
            done();
        });
    });

    it('delete all tracks', function(done) {
    console.log('*******************************************');
        console.log('Deleting all tracks');
        var query = {};
        request(url).delete('/tracks').send(query).end(function(err, res) {
            if(err) {
                throw err;
            }
            console.log(res.body);
            (res.body.status || '').should.equal('ok');
            (res.body.message || false).should.not.equal(false);
            done();
        });
    });

		var trackId;
    it('create track', function(done) {
    console.log('*******************************************');
        console.log('Creating new track');
        var query = {
					date: new Date(),
					locId: locId,
					userId: userId
				};
        request(url).put('/tracks').send(query).end(function(err, res) {
            if(err) {
                throw err;
            }
            console.log(res.body);
            (res.body.status || '').should.equal('ok');
            (res.body.message || false).should.not.equal(false);
            (res.body.track._id ? res.body.track._id : 0).should.not.equal(0);
      trackId = (res.body.track._id ? res.body.track._id : 0);
            done();
        });
    });

    it('get track', function(done) {
    console.log('*******************************************');
        console.log('Getting track: ' + trackId);
        var query = {};
        request(url).get('/tracks/' + trackId).send(query).end(function(err, res) {
            if(err) {
                throw err;
            }
            console.log(res.body);
            (res.body.status || '').should.equal('ok');
            (res.body.message || false).should.not.equal(false);
            (res.body.track._id ? res.body.track._id : 0).should.not.equal(0);
            done();
        });
    });

    it('update track', function(done) {
    console.log('*******************************************');
        console.log('Updating track: ' + trackId);
				var date2 = new Date();
				date2 = date2.setDate(date2.getDate() - 1);
        var query = {
					date: date2
				};
        request(url).post('/tracks/' + trackId).send(query).end(function(err, res) {
            if(err) {
                throw err;
            }
            console.log(res.body);
            (res.body.status || '').should.equal('ok');
            (res.body.message || false).should.not.equal(false);
            (res.body.track._id ? res.body.track._id : 0).should.not.equal(0);
            done();
        });
    });

    it('delete track', function(done) {
    console.log('*******************************************');
        console.log('Deleting track: ' + trackId);
        var query = {};
        request(url).delete('/tracks/' + trackId).send(query).end(function(err, res) {
            if(err) {
                throw err;
            }
            console.log(res.body);
            (res.body.status || '').should.equal('ok');
            (res.body.message || false).should.not.equal(false);
            done();
        });
    });
	

});
